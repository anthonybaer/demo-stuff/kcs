---
bookCollapseSection: false
weight: 70
---

# Compliance Management

In this section we will go over how to properly manage the compliance of
your projects and groups. This allows us to perform separation of
duties.

## Step 1: Setting up Compliance Labels and Pipelines

1. Go to the Workshop group you created

2. Go to **Settings > General**

3. Scroll down to and Expand **Compliance Frameworks**

4. Press the **Add Framework** Button

5. Provide the following and then press the **Add Framework** button

- Name
- Description
- Compliance pipeline configuration: path/file.yaml@group-name/project-name

**Note**
My Compliance Pipeline Configuration is `compliance/.compliance_gdpr.yml@tech-marketing/devsecops/devsecops-workshop/workshop-manifests` which is the path to the yaml in the workshop-manifests project.

## Step 2: Testing Compliance Pipelines

1. Go to the WorkShop Notes Project

2. Click on the **CI/CD** left navigation menu and click on **Pipelines**

3. Click on **Run Pipeline**

4. Ensure that the **main** branch is selected

5. Click on the pipeline, and the new running pipeline should be
the pipeline described in the Compliance Pipeline Configuration file.

## Step 3: Compliance Report

1. Within your group, go to **Security & Compliance > Compliance Report**

Here you'll be able to see recent merge-request activity. Check out
the approval status.

## Step 4: Audit Events

1. Go to **Security & Compliance > Audit Events**

Here you can track important events in your group or project.
The events recorded can be seen here.

## Step 5: Remove Compliance Label

Now we can easily remove the compliance label we added earlier so that our project can run it's regular pipeline.

1. Go to your Project

2. Go to **Settings > General**

3. Click on the **Expand** button in the Compliance framework section

4. Select **None** from the dropdown.

5. Press the **Save changes** button 

---

Congratulations! You just learned how to manage the compliance of your
project and separation of duties.

{{< button relref="/05_appsec_workflow" >}}Previous Lesson{{< /button >}}
{{< button relref="/07_fuzz_testing" >}}Next Lesson{{< /button >}}
