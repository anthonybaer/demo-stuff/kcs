---
bookCollapseSection: false
weight: 20
---

# Prerequisites

In order to get started with GitLab DevSecOps, you will need the following:

- A GitLab account
- A License for GitLab Ultimate
- Google Cloud SDK
- Kubectl
- A Kubernetes Cluster (I am using GKE)

{{< hint info >}}
**Note**
In the future I will try and create an example of setting
up a local cluster and deploying GitLab to that cluster
{{< /hint >}}

## GitLab Account

You may already have a GitLab account, if not you can register here:
https://gitlab.com/users/sign_up

## GitLab Ultimate

In order to get the most out of GitLab DevSecOps, you will require GitLab
Ultimate. If you do not have GitLab Ultimate, you can sign-up for a 30-day trial
license here: https://about.gitlab.com/free-trial/

## Google Cloud SDK

Before starting, you should download and install the Google Cloud SDK. It will allow us to interact with set our cluster via the command-line.

Download it from here: https://cloud.google.com/sdk/docs/quickstart

## KubeCtl

The Kubernetes command-line tool, kubectl, allows you to run commands against Kubernetes clusters. We will need it to interact with the cluster we have created via the CLI.

You can download it from here: https://kubernetes.io/docs/tasks/tools/

## Kubernetes Cluster (GKE)

In this particular tutorial, we will be using a GKE cluster. You can get
$300 towards a GKE cluster when you first sign-up here: https://cloud.google.com/kubernetes-engine

Once you have access to the Google cloud console, you can create a cluster as follows:

1. Go to the Google Cloud Console - console.cloud.google.com
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/1.png)

2. Click on the Menu Tab
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/2.png)

3. Go to the Kubernetes Engine Menu in Google Cloud Platform
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/3.png)

4. Click on the **Create** button
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/4.png)

5. Click on the **Configure** button under GKE Standard
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/5.png)

6. Give the cluster a name
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/6.png)

7. Make sure there are 3 nodes under **Size** in the Node Pools section
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/7.png)

8. In the **Nodes** menu, select the **Series** and a **Machine type** and then
press the **Create** button
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/8.png)

{{< hint info >}}
**Note**
I selected the e2-micro (2 vCPU, 1GB memory) machine, since this workshop doesn't require anything more than that.
{{< /hint >}}

9. Under **Networking** scroll down and select **HTTP load balancing** if it isn't already
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/9.png)

10. Press the **Create** Button

11. Wait for the cluster to render
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/10.png)

12. Click on the rendered cluster
![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/11.png)


13. Click on the **Connect** button

![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/12.png)

14. Copy the **Command-line access** command. Then paste the command into your terminal

![](/devsecops/devsecops-workshop/workshop/pngs/01_prerequisites/13.png)

It should look something like this:

```bash
$ gcloud container clusters get-credentials fern-initech --zone us-central1-c --project fdiaz-02874dfa

Fetching cluster endpoint and auth data.
kubeconfig entry generated for fern-initech.
```

15. Run a simple command to verify the cluster

```bash
$ kubectl cluster-info
Kubernetes master is running at https://104.198.210.9
GLBCDefaultBackend is running at https://104.198.210.9/api/v1/namespaces/kube-system/services/default-http-backend:http/proxy
KubeDNS is running at https://104.198.210.9/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://104.198.210.9/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy
 
$ kubectl get nodes
NAME                                          STATUS   ROLES    AGE   VERSION
gke-fern-initech-default-pool-c3bad177-f4vj   Ready    <none>   53d   v1.22.3-gke.1500
gke-fern-initech-default-pool-c3bad177-pb8d   Ready    <none>   53d   v1.22.3-gke.1500
gke-fern-initech-default-pool-c3bad177-vg5j   Ready    <none>   53d   v1.22.3-gke.1500
```

## Helm

Helm is a package manager for Kubernetes. It makes it easy for us to install
applications containing several Kubernetes manifests, such as Deployments, Services,
Ingress, etc. Let's go ahead an install Helm.

1. Download the installation file

```bash
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
```

2. Make file executable

```bash
$ chmod 700 get_helm.sh
```

3. Run the installation command

```bash
$ ./get_helm.sh
```

## Ingress-Nginx Controller

In order to access our application from the outside world, we need to install an
Ingress Controller. I am choosing Ingress-Nginx since it is supported by the Kubernetes
community. Let's go ahead and install it with helm.

1. Run the installation command

```bash
$ helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace

Release "ingress-nginx" does not exist. Installing it now.
NAME: ingress-nginx
LAST DEPLOYED: Mon May  2 13:09:20 2022
NAMESPACE: ingress-nginx
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed.
It may take a few minutes for the LoadBalancer IP to be available.
You can watch the status by running 'kubectl --namespace ingress-nginx get services -o wide -w ingress-nginx-controller'
```

2. See if pods are coming up

```bash
$ kubectl get pods --namespace=ingress-nginx

NAME                                        READY   STATUS    RESTARTS   AGE
ingress-nginx-controller-5849c9f946-r4l7g   1/1     Running   0          70s
```

{{< hint info >}}
**Note**
I selected the e2-micro (2 vCPU, 1GB memory) machine, since this workshop doesn't require anything more than that.
{{< /hint >}}

3. Obtain the External-IP

```bash
kubectl --namespace ingress-nginx get services -o wide -w ingress-nginx-controller

NAME                       TYPE           CLUSTER-IP    EXTERNAL-IP      PORT(S)                      AGE     SELECTOR
ingress-nginx-controller   LoadBalancer   10.60.12.48   34.134.162.204   80:30688/TCP,443:31926/TCP   2m10s   app.kubernetes.io/component=controller,app.kubernetes.io/instance=ingress-nginx,app.kubernetes.io/name=ingress-nginx
```

The Ingress Controller will automatically create an External-IP using the Load-Balancer
provided by Google. My external IP as seen here is **34.134.162.204**.

---

Congratulations! You have just met all the prerequisites and created a Kubernetes Cluster.

{{< button relref="/" >}}Go Home{{< /button >}}
{{< button relref="/02_deploying_the_demo_application" >}}Next Lesson{{< /button >}}
