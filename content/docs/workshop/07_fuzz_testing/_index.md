---
bookCollapseSection: false
weight: 80
---

# Coverage Based Fuzz-Testing

GitLab allows you to add coverage-guided fuzz testing to your pipelines. This helps you discover bugs and potential security issues that other QA processes may miss. Coverage-guided fuzzing sends random inputs to an instrumented version of your application in an effort to cause unexpected behavior, such as a crash. Such behavior indicates a bug that you should address.

In this section we'll learn how to configure coverage-based fuzzing. We will also go through examining the results.

## Step 1: Configuration

1. Open the **WebIDE**  

2. Use the sidebar to open `.gitlab-ci.yml`

3. Add the **include** section:

```yaml
include:
  - template: Coverage-Fuzzing.gitlab-ci.yml
```

4. Add to the bottom of the file:

```yaml
my_fuzz_target:
  image: python:3.9.1-buster
  extends: .fuzz_base
  stage: c-fuzz
  script:
    - pip install --extra-index-url https://gitlab.com/api/v4/projects/19904939/packages/pypi/simple pythonfuzz
    - ./gitlab-cov-fuzz run --engine pythonfuzz -- fuzz.py
```

5. Update the stages in `.gitlab-ci.yml`

```yaml
stages:
    - build
    - c-fuzz
    - deploy
```

## Step 2: Adding a vulnerability

{{< expand "Animated GIF" "expand" >}}
![Coverage Based Fuzz-Testing Vuln](/devsecops/devsecops-workshop/workshop/gifs/lesson_7_02.gif)
{{< /expand >}}

1. Open `fuzz.py` under the root directory and replace the file contents with the following:

```python
from html.parser import HTMLParser
from pythonfuzz.main import PythonFuzz

@PythonFuzz
def fuzz(buf):
    try:
        string = buf.decode("ascii")
        parser = HTMLParser()
        parser.feed(string)
    except UnicodeDecodeError:
        pass

if __name__ == '__main__':
    fuzz()
```

2. Select Commit

3. Select **Create a new branch**, give the branch a name, check **Start a new merge request** and press the **Commit** button

4. Give the merge request a name and description

5. Press the **Create merge request** button

6. Wait for the pipeline to complete

{{< hint info >}}
**NOTE**
The c-fuzz job will show failure, but this is to be expected, we can see the results once the job completes.
{{< /hint >}}

## Step 3: Viewing the Results in MR

{{< expand "Animated GIF" "expand" >}}
![Coverage Based Fuzz-Testing Results](/devsecops/devsecops-workshop/workshop/gifs/lesson_7_03.gif)
{{< /expand >}}

1. Once the pipeline has complete, we can expand the **Security scanning** section

{{< hint info >}}
**NOTE**
You may need to refresh the page
{{< /hint >}}

2. Select the **Uncaught-exception** Vulnerability

3. Here we can see the vulnerability in full detail

Here you can see that an un-handled exception was picked up. This is because we are just passing instead of performing a function.

---

# Web-API Based Fuzz-Testing

GitLab allows you to add web-api fuzz testing to your pipelines. This helps you discover bugs and potential security issues that other QA processes may miss. API fuzzing performs fuzz testing of API operation parameters. Fuzz testing sets operation parameters to unexpected values in an effort to cause unexpected behavior and errors in the API back-end.

In this section we'll learn how to configure Web-API based fuzzing. We will also go through examining the results. 

## Step 1: Configuration

{{< expand "Animated GIF" "expand" >}}
![Web API Fuzz-Testing Configuration](/devsecops/devsecops-workshop/workshop/gifs/lesson_7_04.gif)
{{< /expand >}}

1. Open the **WebIDE**  

2. Create a new file named `test_openapi.v2.0.json` with the OpenAPI spec seen below in the project's root directory, making sure that **INGRESS_ENDPOINT** is replaced with your Ingress Endpoint obtained in lesson 2.

```json
  {
  "swagger": "2.0",
  "info": {
    "version": "1.0",
    "title": "Adding Notes",
    "description": "Testing adding notes via simply simple notes"
  },
  "host": "INGRESS_ENDPOINT",
  "basePath": "/notes",
  "schemes": [
    "http"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/add": {
      "post": {
        "description": "User provides a note to add to the database",
        "summary": "POST api to create a note",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "message",
            "in": "body",
            "required": true,
            "description": "The user provided text used to create a note",
            "schema": {
              "$ref": "#/definitions/CreateNoteRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Note added successfully!"
          }
        }
      }
    }
  },
  "definitions": {
    "CreateNoteRequest": {
      "title": "Create NoteRequest",
      "example": {
        "message": "meow"
      },
      "type": "object",
      "properties": {
        "message": {
          "description": "The user provided text used to create a note",
          "example": "meow",
          "type": "string"
        }
      },
      "required": [
        "message"
      ]
    }
  }
}
```

3. Add the following template to `.gitlab-ci.yml`

```yaml
include:
  - template: API-Fuzzing.gitlab-ci.yml
```

3. Add the following variables to `.gitlab-ci.yml`, remember to replace
the **INGRESS_ENDPOINT** with your ingress endpoint

```yaml
variables:
  FUZZAPI_PROFILE: Quick-10
  FUZZAPI_OPENAPI: test_openapi.v2.0.json
  FUZZAPI_TARGET_URL: http://INGRESS_ENDPOINT
```

7. Update the stages in `.gitlab-ci.yml`

```yaml
stages:
  - fuzz
```

8. Select Commit

9. Select commit to master

## Step 2: Adding a vulnerability

Now let's add a vulnerability to see Web API fuzzing in action.

{{< expand "Animated GIF" "expand" >}}
![Web API Fuzz-Testing Vulnerability](/devsecops/devsecops-workshop/workshop/gifs/lesson_7_05.gif)
{{< /expand >}}

1. Open the **WebIDE**

2. Select **notes/routes.py**

3. Change the following in the `add_note()` function in `notes/routes.py`

```python
if len(msg) > 1024:
        return jsonify({"Error": "Message tooooo long!"}), 400
```

to

```python
if len(msg) > 10:
        return jsonify({"Error": "Message tooooo long!"}), 500
```

4. Press the **Commit** button

5. Verify the code

6. Select **Create a new branch** and **Start a new merge request**, and press **Commit**

7. Give the MR a title and a description

8. Press the **Submit merge request** button

## Step 3: Viewing the Results

{{< expand "Animated GIF" "expand" >}}
![Web API Fuzz-Testing Results](/devsecops/devsecops-workshop/workshop/gifs/lesson_7_06.gif)
{{< /expand >}}

1. When the pipeline completes, press **Expand** under the **Security Scan** section

{{< hint info >}}
**Note**
You may need to refresh your browser
{{< /hint >}}

2. Click on the **Unknown Fuzzing injection via 'message' on 'POST'** vulnerability

3. Here you have information on the Exception caught by fuzzing

Here you can see that an un-handled exception was picked up. This is because the server threw a 500 instead of handling the error. Ideally a database error should cause the 500, this is just a forced test.

---

Congratulations! You have now successfully learned how to perform both
Coverage-based and Web-API fuzzing on your application.

{{< button relref="/06_compliance_management" >}}Previous Lesson{{< /button >}}
{{< button relref="/08_other_topics" >}}Next Lesson{{< /button >}}
