---
bookCollapseSection: false
weight: 30
---

# Deploying an Application with GitLab Pipeline

In this lesson, we will clone the project over to our space, so that we can make
edits. We will deploy our application to our Kubernetes cluster, along with
an Ingress controller. This will allow us to access the application from the outside world.

## Step 1: Create a new Workshop Group

1. From the GitLab UI, Click on the **Menu** tab
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/1.png)

2. Go to the **Groups** tab
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/2.png)

3. Click on the **Create Group** tab
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/3.png)

4. Press the **Create group** button
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/4.png)

5. Give the group a name, and press the **Create group** button
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/5.png)

6. Now you have a group in which we can start adding projects to
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/6.png)

## Step 2: Cloning the Sample Project

Here we will clone the sample project which we will use through this workshop. It's a simple python Flask application which add/removes notes from a MariaDB DataBase.

1. Press the **New Project** button

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/7.png)

{{< hint info >}}
**Note**
This action must be done within the group created in Step 1.
{{< /hint >}}

2. Select **Import project**

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/8.png)

3. Press the **Repo By URL** button

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/9.png)

4. Under **Git repository URL** add the following URL:

```text
https://gitlab.com/tech-marketing/devsecops/devsecops-workshop/workshop-notes.git
```

5. Select **Public** under visibility level

6. Press the **Create project** button
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/10.png)

7. Wait for the project to be imported. It will take a few seconds
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/11.png)

{{< hint info >}}
**Note**
You should be redirected to the newely imported project along with
the message "The project was successfully imported"

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/12.png)
{{< /hint >}}

## Step 3. Cloning the Compliance and Deployment Manifests

Here we will clone the sample project which house the files used to deploy
the ingress controller, and echo application. It's a collection of compliance
pipeline and deployment yamls.

1. Go back to the group created in step 1
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/13.png)

2. Press the **New Project** button

3. Select **Import project**
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/14.png)

4. Press the **Repo By URL** button
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/15.png)

5. Under **Git repository URL** add the following URL:

```text
https://gitlab.com/tech-marketing/devsecops/devsecops-workshop/workshop-manifests.git
```

6. Select **Public** under visibility level

7. Press the **Create project** button
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/16.png)

7. Wait for the project to be imported. It will take a few seconds
![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/17.png)

{{< hint info >}}
**Note**
You should be redirected to the newely imported project along with the message "The project was successfully imported"
{{< /hint >}}

## Step 4. Setting up the Project to use the Cluster

In this section we will be installing the GitLab [Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/) to interact with the cluster and deploy our Kubernetes manifests.

1. Open the **WebIDE**

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/18.png)

{{< hint info >}}
**Note**
This will be done within the **workshop-notes** project.
{{< /hint >}}

2. Open the file `.gitlab/agents/kube-agent/config.yaml` which contains the following:

```yaml
gitops:
  manifest_projects:
  - id: path/to/workshop-manifests/project
    paths:
    - glob: '/deploy/*.yaml'
```

and make sure to configure the path appropriately to your project.

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/19.png)

{{< hint info >}}
**Note**
The id for my workshop-notes is `fjdiaz/workshop-manifests`
{{< /hint >}}

3. Commit this change by clicking on the **Commit** button

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/20.png)

4. Click on **Commit to main branch** and then press the **Commit** button. You can now deploy the Kubernetes agent to your cluster

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/21.png)

5. Click on the **Infrastructure > Kubernetes clusters** in the left navigation menu

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/22.png)

6. Click on the **Install new Agent** button

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/23.png)

7. Select your agent (kube-agent) from the drop down and press the **Register** button

![](/devsecops/devsecops-workshop/workshop/pngs/02_deploying_the_demo_application/24.png)

8. Open a terminal and connect to your cluster

```bash
$ gcloud container clusters get-credentials fern-initech --zone us-central1-c --project fdiaz-02874dfa

Fetching cluster endpoint and auth data.
kubeconfig entry generated for fern-initech.
```

9. Run the following command to deploy the agent onto your cluster

```bash
$ 
```

{{< hint info >}}
**Note**
This will deploy the kubernetes-agent which will then start deploying
items found in the deploy folder in the agents configuration
{{< /hint >}}

10. Verify the Kubernetes Agent is running

```bash
$ kubectl get pods -n gitlab-kubernetes-agent
```

## Step 5: Running the Pipelines

Now let's run a pipeline to deploy the application to our Kubernetes cluster.

{{< expand "Animated GIF" "expand" >}}
![Run Pipeline](/devsecops/devsecops-workshop/workshop/gifs/lesson_2_05.gif)
{{< /expand >}}

1. Click on the **CI/CD** left navigation menu and click on **Pipelines**

2. Click on **Run Pipeline**

3. Ensure that the **main** branch is selected

4. Press the **Run Pipeline** button

{{< hint info >}}
**Note**
You should now see the pipeline running on your project
{{< /hint >}}

## Step 6: Accessing our application

Now let's use the ingress to access our application. With the default settings
your application should be avaliable at your Load-Balancers IP under the `/notes` path. These items can be configured via the [values.yaml](https://gitlab.com/tech-marketing/devsecops/devsecops-workshop/workshop-notes/-/blob/main/helm/values.yaml) within
the helm path.

{{< expand "Animated GIF" "expand" >}}
![Access Application](/devsecops/devsecops-workshop/workshop/gifs/lesson_2_06.gif)
{{< /expand >}}

1. Get the Load-Balancer External IP-Address

```bash
$ kubectl get svc -n ingress-nginx
```

2. Point your browser to http://[Load-Balancer External IP-Address]/notes
---

Congratulations! You have now successfully deployed an application using GitLab CICD.

{{< button relref="/01_prerequisites" >}}Previous Lesson{{< /button >}}
{{< button relref="/03_setting_up_and_configuring_the_security_scanners" >}}Next Lesson{{< /button >}}
