---
bookCollapseSection: false
weight: 80
---

# Other Topics

In this Section, I'll touch upon some a few other topics which
you can leverage to get full usage out of GitLab's Security Solutions.

## OnDemand Scans

{{< expand "GIF" "expand" >}}
## ADD GIF HERE
{{< /expand >}}

On-Demand Scans can be used to run DAST whenever we
choose as well as on a schedule.

1. Go to **Security & Compliance > On-Demand Scans**

You will be taken to the On-demand scans list which
contains a list of scans which have been run in the
past with additional details.

2. Click on the **New DAST scan** button

3. Provide a **Scan name** and **Description**

4. Select a Scanner profile or create your own

5. Select a Site profile or create your own

6. Click on the **Schedule scan** box and select
a Start time.

7. Press **Save and run scan**

## Dependency List

{{< expand "GIF" "expand" >}}
## ADD GIF HERE
{{< /expand >}}

1. Make sure you have **Dependency Scanning** enabled on
the main branch.

2. Go to **Security & Compliance > Dependency List**

Here you can see a list of Dependencies which includes the
following sections:

- Component
- Packager
- Location
- License
- Vulnerabilties Detected 

## GraphQL API

{{< expand "GIF" "expand" >}}
## ADD GIF HERE
{{< /expand >}}

With GitLab's GraphQL API, we can perform functions on Security Vulnerabilites. This makes it easy to write our own reporting tools.

1. Create a Personal Access Token

2. 

3. 

---

Congratulations! You have now successfully completed this workshop. Want to Contribute and add to this project? See the [Contribution Documentation]().

{{< button relref="/" >}}Go Home{{< /button >}}
{{< button relref="/07_fuzz_testing" >}}Previous Lesson{{< /button >}}
