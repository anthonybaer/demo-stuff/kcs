---
bookCollapseSection: false
---

# Contributing

Want to contribute to this workshop or project? You can do so in a couple
of different ways. Be sure to see the [architecture documentation]() and the [development guide]()
and then you can:

1. **Open an issue if you find something wrong/missing within the workshop**
2. **Open an MR with changes**
3. **Open an issue with a Feature Request**
