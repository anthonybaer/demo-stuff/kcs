---
bookCollapseSection: false
---

# Workshop Notes Application Architecture

This page contains the architecture of the Workshop Notes application. It
is an application developed in Python using the Flask Web Framework.

## Architecture Diagram

![Architecture Diagram](/devsecops/devsecops-workshop/workshop/diagrams/architecture_diagram.png)

## API Diagram

![API Diagram](/devsecops/devsecops-workshop/workshop/diagrams/api_diagram.png)

## Dependencies

- Flask
