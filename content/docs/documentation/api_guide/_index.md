---
bookCollapseSection: false
---

# API Guide

This is a guide letting you know the functions avaliable via the API. You can perform Create, Delete, and View
Operations on Notes using the API.

## Create a Note

```bash
$ curl -X POST http://{LoadBalancer_IP}/api/v1/notes -d '{"message": "ZAP"}'

{
  "Sucess": "Note added!"
}

$ curl -X POST http://{LoadBalancer_IP}/api/v1/notes -d '{"message": "YEET"}'

{
  "Sucess": "Note added!"
}
```

## View all Notes

```bash
$ curl -X GET http://{LoadBalancer_IP}/api/v1/notes

[(1, 'ZAP'), (2, 'YEET')]
```

## View select Note

```bash
$ curl -X GET http://{LoadBalancer_IP}/api/v1/notes\?id\=1

[(1, 'ZAP')]
```

## Delete select Note

```bash
$ curl -X DELETE http://{LoadBalancer_IP}/api/v1/notes\?id\=1

Note deleted successfully!
```
