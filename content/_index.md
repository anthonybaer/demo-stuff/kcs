---
title: Introduction
type: docs
---

# Welcome

Welcome to the GitLab DevSecOps and Compliance Management Workshop.
It will help you gain a better understanding of how to successfully
shift security left to find and fix security flaws during development
and to do so more easily and with greater visibility and control than
typical approaches can provide.

## Getting Started

In order to get started, go through each of the lessons described within the workshop. Be sure to start with the prerequisites.

To learn about the project we are using you can see the following documentation:

- [Project Architecture]()
- [Development Guide]()
- [Contributing]()

## Outcomes

- How to achieve comprehensive security scanning without adding a bunch of new tools and processes
- How to secure and protect your cloud native applications and IaC environments within existing DevOps workflows
- How to use a single-source-of-truth to improve collaboration between dev and sec
- How to manage all of your software vulnerabilities in one place
- How to automate and monitor your security policies and simplify auditing
- How to set up compliance pipelines to provide consistent guardrails for developers
- How to detect unknown vulnerabilities and errors using fuzz-testing
- How to use the GraphQL API to manage vulnerabilities

## Sections

| # |     Title     |                Description                   |
| - |---------------|----------------------------------------------|
| 1 | Prerequisites | Requirements to get started with the project |
| 2 | Deploying the Demo Application | Learn how to deploy and expose the demo application |
| 3 | Setting up and Configuring the Security Scanners | Learn how to setup and configure the different types of security scans. This includes Security Policies as well |
| 4 | Developer Workflow | Learn how to view and take action on vulnerabilities within a Merge Request |
| 5 | AppSec Workflow | Learn how to triage vulnerabilities and collaborate with other members of a Security team |
| 6 | Compliance Management | Learn how to setup Compliance Pipelines, Verify the Compliance Status of your project, and Audit Reports |
| 7 | Fuzz Testing | Use Coverage-Based and Web-API Fuzz Testing to detect vulnerabilities and errors in your code |
| 8 | Other Topics | Learn Other Topics such as OnDemand Scans, Using the GraphQL API, and more |