# GitLab DevSecOps Workshop

Welcome to the GitLab DevSecOps + Compliance Management Workshop. You can access the
workshop by visiting https://tech-marketing.gitlab.io/devsecops/devsecops-workshop/workshop/

---

## Running Locally

In order to run this Hugo WebSite locally, you can follow these
steps:

1. Install [Go](https://go.dev/)

2. Install Hugo

```bash
$ git clone https://github.com/gohugoio/hugo.git
$ cd hugo
$ go install --tags extended
```

3. Run hugo

```bash
hugo server -D
```

